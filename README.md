# demo-aggregate-agere16 #

This repo includes the code for the scafi simulation corresponding to the case study presented in the paper "Programming actor-based collective adaptive systems".

To start the simulation, simply run:


```
#!bash

$ sbt
> runMain AgereDemoMain
```

Notes:

* The hotkey "2" can be used to turn on the sensor for signalling a failure in a device.